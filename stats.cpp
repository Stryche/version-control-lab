#include <iostream>
using namespace std;

int main()
{
  int x, y, sum, difference, product, distance;
  double mean;

  cout << "Please enter two numbers: ";
  cin >> x >> y;

  sum = x + y;
  difference = x - y;
  product = x * y;
  distance = y - x;
  mean = sum / 2.0;

  cout << "The sum of " << x << " and " << y << " is: " << sum << "\n";
  cout << "The difference of " << x << " and " << y << " is: " << difference << "\n";
  cout << "The product of " << x << " and " << y << " is: " << product << "\n";
  cout << "The distance of " << x << " and " << y << " is: " << distance << "\n";
  cout << "The mean of " << x << " and " << y << " is: " << mean << "\n";

  return 0;
  
}
